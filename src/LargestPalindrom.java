public class LargestPalindrom {

	public static void main(String[] args) {
		int numToTest = 90;
		int reverseOfTest = reverse(numToTest);
		System.out.println(reverseOfTest);
	}

	private static int reverse(int numToTest) {
		int temp = 0;
		int sum = 0;
		while (numToTest != 0) {
			temp = numToTest % 10;
			sum*=10;
			sum += temp;
			numToTest /= 10;
		}

		return sum;
	}

}
