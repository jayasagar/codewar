public class practice {

	public static void main(String[] args) {
		int temp = 0;
		for (int n = 1; n < 1000; n++) {
			if (n % 3 == 0 || n % 5 == 0) {
				temp += n;
			}
		}
		System.out.println(temp);
	}

}
