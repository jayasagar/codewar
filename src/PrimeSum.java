import java.util.LinkedList;

public class PrimeSum {

	public static void main(String[] args) throws Exception {
		try {
			System.out.println(countPrimesFast(2000000));
		} catch (Exception e) {
			System.out.println("Surreounded by catch");
			e.printStackTrace();
		}
	}



private static double countPrimesFast(int count) throws Exception{ 
if(count < 10){ 
throw new IllegalArgumentException("method only works for count >= 10"); 
} 
long start = System.currentTimeMillis(); 
long sum = 2 + 3 + 5 + 7; 
int[] step = {1, 3, 7, 9}; 
LinkedList<Integer> previousPrimes = new LinkedList<Integer>(); 
previousPrimes.add(3); 
previousPrimes.add(5); 
previousPrimes.add(7); 
for(int i = 10; i < count; i += 10){ 
step: 
for(int j : step){ 
int k = i + j; 
double sqrt = Math.sqrt(k); 
for(int prevPrime : previousPrimes){ 
if(prevPrime > sqrt){ 
break; 
} 
if(k % prevPrime == 0){ 
continue step; 
} 
} 
sum+= k; 
previousPrimes.add(k); 
} 
} 
System.out.println(sum + " sum "); 
return System.currentTimeMillis() - start; 
}
}