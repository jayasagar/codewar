/*
 * program to calculate the sum of even fibonacci numbers below 4000000: 1,2,3,5
 */
public class EvenFibonacci {
	public static void main(String[] args) {
		int fib = 1;
		int a = 1;
		int b = 1;
		int sum = 0;
		while (fib <= 4000000) {
			System.out.println(fib);
			if (fib % 2 == 0)
				sum += fib;
			fib = a + b;
			b = a;
			a = fib;
		}
		System.out.println("Sum:" + sum);

	}

}
