public class NumberPrime {

	public static void main(String[] args) {
		int primePosition = 9999999;
		long prime = findPrimePosition(primePosition);

		System.out.println(prime);
	}

	private static long findPrimePosition(int primePosition) {
		long temp = 3;
		int count = 1;
		while (count <= primePosition) {
			temp += 2;
			if (isPrime(temp))
				count++;
		}
		return temp;
	}

	private static boolean isPrime(long temp) {
		for (int i = 3; i < temp / 2; i += 2) {
			if (temp % i == 0)
				return false;
		}
		return true;
	}

}
