public class LargestPrimeFactor {

	public static void main(String[] args) {
		/*
		 * Find odd factor, see if prime find the largest
		 */
		boolean flag;
		int max = 0;

		int num = 13195;
		for (int i = 3; i < num / 2; i += 2) {
			if (num % i == 0) {
				System.out.println("Odd Factor:" + i);
				flag = checkFactorIsPrime(i);
				if (flag) {
					if (max < i) {
						max = i;
					}
				}
			}
		}
		System.out.println("There you go, Largest Prime Factor is :" + max);
	}

	private static boolean checkFactorIsPrime(int i) {
		for (int j = 3; j < i; j++) {
			if (i % j == 0) {
				System.out.println("Factor:" + i + "is not prime");
				return false;
			}
		}
		return true;
	}
}
