package com.geeklabs.ds.basic;

public class SumOfNNumbers {

	public static void main(String[] args) {
		final int n=5;
		int sum=0;
		/*for(int i=1; i<=n;i++){ // TO = O(n) 
			sum+=i;			
		}*/
		sum=n*(n+1)/2;
		System.out.println(sum);
	}
}
