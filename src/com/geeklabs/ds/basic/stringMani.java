package com.geeklabs.ds.basic;

public class stringMani {

	public static void main(String[] args) {
		System.out.println(reverseEach("This%is%my%promise%to%you"));
			
			
		}

	/*private static String reverseEach(String string) {
		String[] parts = string.split("'%'");
		StringBuilder finalString = new StringBuilder();
		
		for(String p:parts){
			StringBuilder reverse= new StringBuilder(p).reverse();
			finalString.append(reverse);
			finalString.append(' ');
			
		}
		return finalString.toString();
		
	}*/

	
	

public static String reverseEach(String inputString) {
	  String[] parts = inputString.split(" ");
	  StringBuilder sb = new StringBuilder();
	  for (String p : parts) {
	    sb.append(new StringBuffer(p).reverse().toString());
	    sb.append(' ');
	   }
	   return sb.toString();
}
}