package com.geeklabs.ds.basic;

public class ReverseAnArray {

	public static void main(String[] args) {
	String[] arrayExample={"a","sareta","m","party"};
	int length= arrayExample.length;
	for(int i=0; i<length/2; i++){
		String temp= arrayExample[i];
		arrayExample[i]=arrayExample[length-(i+1)];
		arrayExample[length-(i+1)]=temp;
	}
	for(int i=0; i<length; i++){
		System.out.println(arrayExample[i]);
	}
	}

	public static void swap(String str1, String str2) {
		String swapped = str2;
		str2 =str1;
		str1 = swapped;
	}
	

	
}
