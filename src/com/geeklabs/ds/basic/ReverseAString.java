package com.geeklabs.ds.basic;

/*
 * 
 */
public class ReverseAString {
	public static void main(String[] args) {
		String example="Try";
		String s = new String("Try");
		StringBuilder reverse= new StringBuilder();
		int len=example.length();
		/*for(int i=0; i<len; i++){
			reverse =reverse+example.charAt(len-i-1);
		}*/
		for(int i=len-1; i>=0; i--){
			reverse = reverse.append(example.charAt(i));
		}
		System.out.println(reverse);
	}

	 public ReverseAString(){
		 super();
	 }
}

/*
  1. javac ReserveAString.java -> compiler creates a .class file and puts in the file system.
  
  2. java ReverseAString->instance of JVM is created
  
  3. JVM will load the .class, native code, string constant pool, file into the non-heap space.
  
  4. JVM verification takes place. Then it loads the class file.
  5. Why static is required on jvm??
  


*/