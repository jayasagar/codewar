package com.geeklabs.ds.basic;
import java.util.HashMap;
import java.util.Map;


public class NoOfOccurences {

	public static void main(String[] args) {
		int[] num = new int[5];//declaration and instantiation, JVM allots 32 bit*5 times mem block,contiguous mem location
		num[0]=2;
		num[1]=2;
		num[2]=5;
		num[3]=5;
		num[4]=8;
		
		Map<Integer,Integer> noOfOccurence = new HashMap<Integer,Integer>();
		Integer temp=0; 
		for(int i=0;i<num.length;i++){
			// Get element from array			
			// check whether that element exist in hashmap
			if(noOfOccurence.containsKey(num[i])) {// O(1)
				temp = noOfOccurence.get(num[i]);temp++;// O(1)
				noOfOccurence.put(num[i], temp);//O(1)
			}else {
				noOfOccurence.put(num[i], 1);
			}
			// if exist , get a value and increment it by 1 and again out it back
			// else , simply put with value as 1		
		}
		System.out.println(noOfOccurence);
		// O(n)
	}

}
