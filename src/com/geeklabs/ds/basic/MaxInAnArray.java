package com.geeklabs.ds.basic;

public class MaxInAnArray {
	public static void main(String[] args) {
		int[] arrayOfNum = {2,20,4,8,23};
		int max=0;
		for(int i=0;i<arrayOfNum.length;i++){
			if(arrayOfNum[i]>max){
				max=arrayOfNum[i];
			}
		}
		System.out.println(max);
		
	}

}
