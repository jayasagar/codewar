package com.geeklabs.ds.hash;

public class HashMapTest {

	public static void main(String[] args) {
		HashMapImpl hashMapTest= new HashMapImpl();
		hashMapTest.put(1, "Austin");
		hashMapTest.put(2, "San Francisco");
		hashMapTest.put(3, "Chicago");
		System.out.println(hashMapTest);
		
		System.out.println(hashMapTest.get(3));
	}

}
