package com.geeklabs.ds.hash;

public class HashMapImpl {
	private static final int SIZE = 10;
	Entry entryList[] = new Entry[SIZE];

	class Entry {
		Object key;
		Object value;
		Entry next;

		Entry(Object key, Object val) {
			this.key = key;
			this.value = val;
		}
	}

	public void put(Object key, Object val) {
		int hashIndex = key.hashCode() % SIZE;
		Entry entry = entryList[hashIndex];
		if (entry != null) {
			if (entry.key.equals(key)) {
				entry.value = val;
			} else {
				while (entry.next != null) {
					entry = entry.next;
				}
				entry.next = new Entry(key, val);
			}
		} else {
			entry = new Entry(key, val);
			entryList[hashIndex] = entry;
		}
	}

	public Object get(Object key) {
		int hashIndex = key.hashCode() % SIZE;
		Entry entry = entryList[hashIndex];
		while (entry != null) {
			if (entry.key.equals(key)) {
				return entry.value;
			}
			entry = entry.next;
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder("[");
		for (int i = 0; i < entryList.length; i++) {
			Entry temp = entryList[i];
			if (temp != null) {
				str.append(" key:" + temp.key + " value:" + temp.value);
			}
		}
		str.append("]");
		return str.toString();
	}
}