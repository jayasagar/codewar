package com.sareeta.self.practice;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class HashMapPractice {

	public static void main(String[] args) {
		Map<String,Double> map = new HashMap<String,Double>();
		map.put("John Doe", new Double(3434.34));
		Set<Entry<String, Double>> set = map.entrySet();
		Iterator<Entry<String, Double>> itr = set.iterator();
		while (itr.hasNext()) {
			Entry<String, Double> val = itr.next();
			System.out.println(val.getKey()+" " +val.getValue());
		}

	}

}
