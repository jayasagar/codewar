package com.sareeta.self.practice;

import java.util.HashSet;
import java.util.Set;

public class SetPractice {

	public static void main(String[] args) {
		Boolean[] ba = new Boolean[5];
		Set set = new HashSet();
		ba[0] = set.add("a");
		ba[1] = set.add("a");
		ba[2] = set.add("b");
		ba[3] = set.add("c");
		ba[4] = set.add("a");
		for (Object o : ba)
			System.out.println(o);
		for (Object o : set)
			System.out.println(o);
	}

}
