package com.sareeta.self.practice;

import java.util.ArrayList;
import java.util.List;

public class SortingArrayWithCollections {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(5);
		list.add(6);
		list.add(3);

		System.out.println(list.size());
		Object[] arrayforTest = list.toArray();

		for (int i = 0; i < arrayforTest.length; i++) {
			System.out.println(arrayforTest[i]);
		}
	}

}
