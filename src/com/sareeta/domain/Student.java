package com.sareeta.domain;

public class Student implements Comparable<Student> {
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private int age;
	private int id;
	private String name;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (id != other.id)
			return false;
		return true;
	}

	/*
	 * public int compareTo(Student stu) { return stu.getName().compareTo(name);
	 * }
	 */

	@Override
	public int compareTo(Student o) {
		if (o.getAge() > age)
			return 1;
		else if (o.getAge() < age)
			return -1;
		else
			return 0;
	}

}
