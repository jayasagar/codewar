package com.sareeta.collections.compareTo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import com.sareeta.domain.Student;

public class CompareToTest {
	public static void main(String[] args) {
		Student stu = new Student();
		Student stu1 = new Student();
		Student stu2 = new Student();
		Student stu3 = new Student();
		stu.setAge(23);
		stu1.setAge(5);
		stu2.setAge(45);
		stu3.setAge(4);
		List<Student> studentList = new ArrayList<Student>();
		studentList.add(stu);
		studentList.add(stu1);
		studentList.add(stu2);
		studentList.add(stu3);
		Iterator<Student> itr = studentList.iterator();
		while(itr.hasNext()){
			System.out.println((itr.next()).getAge());
		}
		Collections.sort(studentList);
		System.out.println("After Sorting:");
		for(Student o : studentList){
			System.out.println(o.getAge());
		}

	}

}
