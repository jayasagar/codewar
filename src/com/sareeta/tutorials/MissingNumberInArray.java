package com.sareeta.tutorials;

public class MissingNumberInArray {

	public static void main(String[] args) {
		int[] arrayToTraverse = { 1, 3, 27 };
		int i = 0;
		while (true) {
			if (i == arrayToTraverse.length - 1) {
				break;
			}
			int temp = arrayToTraverse[i + 1] - arrayToTraverse[i];
			if (temp > 1) {
				for (int j = 1; j < temp; j++) {
					System.out.println(arrayToTraverse[i] + j);
				}
			}
			i++;
		}
	}

}
