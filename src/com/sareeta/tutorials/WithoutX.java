package com.sareeta.tutorials;
/*
 * Given a string, if the first or last chars are 'x', return the string without those 'x' chars, and otherwise return the string unchanged. 

withoutX("xHix") → "Hi"
withoutX("xHi") → "Hi"
withoutX("Hxix") → "Hxi"
 */
public class WithoutX {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
