package com.sareeta.tutorials;

public class LongestPalindromeInString {

	public static void main(String[] args) {
		String str = "itnautmaamtuaitn";
		String largestPalindrome = "";
		int maxLength = 0;
		for (int i = 0; i < str.length(); i++) {
			for (int j = str.length(); j > i; j--) {
				String str2 = str.substring(i, j);
				if (maxLength < str2.length() && isPalindrome(str2)) {
					largestPalindrome = str2;
					maxLength = largestPalindrome.length();
				}
			}
		}
		if (maxLength > 0)
			System.out.println("Largest Palindrome : " + largestPalindrome
					+ " with length: " + maxLength);
		else
			System.out.println("No Palindrome in your string");
	}

	public static boolean isPalindrome(String str) {
		boolean flag = true;
		int length = str.length();
		for (int i = 0; i < length / 2; i++) {
			if (str.charAt(i) != str.charAt(length - (i + 1))) {
				flag = false;
				break;
			} else {
				continue;
			}
		}
		return flag;
	}

}
