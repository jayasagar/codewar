package com.sareeta.tutorials;

//Use Generics, Add exceptions, Multithread, memory leak, cloneable,serializable

public class Stack<E> {
	private final Object[] elements;
	private int top=-1;
	
    public Stack(int capacity){
    elements = new Object[capacity];
    }
    
	//insert elements
	public boolean push(E eleToPush){
		if(!isFull()){
		elements[++top] = eleToPush; 
		return true;
		}
		else {
		return false;
		}
	}
	
	//remove elements,returns -1 when empty
	public E pop(){
		if(!isEmpty()){
		return (E)elements[top--];
		}
		else{
			return null;
		}
	}
	//check if no elements
	public boolean isEmpty(){
		return (top==-1);	
	}
	
	//check if full
	public boolean isFull(){
		return (top==elements.length-1);
		
	}
	
	public void display(){
		for(int i=0; i<elements.length; i++){
			System.out.println(elements[i]);
		}
		
	}

}
