package com.sareeta.tutorials;

public class Palindrome {

	public static void main(String[] args) {
		String isPalindrome = "dghdhdgMadaMdhjdhjdhjdhj";
		boolean flag=true;
		int length = isPalindrome.length();
		for(int i=0; i<length/2; i++){
			if(isPalindrome.charAt(i)!=isPalindrome.charAt(length-(i+1))){
				flag=false;
				break;
			}else{
				continue;
			}			
		}
		if(flag){
			System.out.println("It's palindrome");
			}
	}
	}
