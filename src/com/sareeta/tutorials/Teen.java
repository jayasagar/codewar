package com.sareeta.tutorials;

public class Teen {

	public static void main(String[] args) {
		System.out.println(loneTeen(99, 99));
	}

	public static boolean loneTeen(int a, int b) {
		return (!((a >= 13 && a <= 19) && (b >= 13 && b <= 19)));
	}

}
