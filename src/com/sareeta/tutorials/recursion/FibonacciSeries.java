package com.sareeta.tutorials.recursion;

public class FibonacciSeries {

	public static void main(String[] args) {
		int elementOfTheFibonacciSeries = 6;
		System.out.println("The " + (elementOfTheFibonacciSeries+1)
				+ "th element of hte Fibonacci series is:"
				+ findFibonacci(elementOfTheFibonacciSeries));

	}

	private static int findFibonacci(int fibonaccival) {

		if(fibonaccival ==0 ||fibonaccival==1)
			return 1;
		
	 return findFibonacci(fibonaccival-1) + findFibonacci(fibonaccival-2);

	}

}
