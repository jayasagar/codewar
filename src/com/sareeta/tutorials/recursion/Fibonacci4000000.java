package com.sareeta.tutorials.recursion;

public class Fibonacci4000000 {

	public static void main(String[] args) {
		int elementOfTheFibonacciSeries = 200;
		System.out.println("The " + (elementOfTheFibonacciSeries + 1)
				+ "th element of hte Fibonacci series is:"
				+ findFibonacci(elementOfTheFibonacciSeries));

	}

	private static int findFibonacci(int fibonaccival) {

		if (fibonaccival == 0 || fibonaccival == 1)
			return 1;

		return findFibonacci(fibonaccival - 1)
				+ findFibonacci(fibonaccival - 2);

	}

}
