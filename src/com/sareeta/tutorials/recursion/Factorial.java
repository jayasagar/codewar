package com.sareeta.tutorials.recursion;

public class Factorial {

	public static void main(String[] args) {
		int valForFactorial = 5;
		System.out.println("The factorial of " + valForFactorial + " is : "
				+ findFactorial(valForFactorial));
	}

	private static int findFactorial(int valForFactorial) {

		if (valForFactorial == 0 || valForFactorial == 1) {
			return 1;
		}

		return valForFactorial * findFactorial(valForFactorial - 1);
	}

}
