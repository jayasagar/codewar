package com.sareeta.tutorials;
public class UniqueCharacters {
	public static void main(String[] args) {
		String str= "mass";
		boolean flag=false;
		flag = approach1(str);		
			if (flag){ System.out.println("Unique");}
			else {
				System.out.println("Not Unique");
			}
	}
	
	public static boolean approach1(String stringToVerifyIfUniqueChars){
		//create an array of boolean of 256 length
		boolean[] arrayToStoreIntValOfChars = new boolean[256];
		//take each char of the string
		for(int i=0 ; i<stringToVerifyIfUniqueChars.length();i++){
			//check if the character's val is true already in the array			
			int charAt = (int)stringToVerifyIfUniqueChars.charAt(i);
			if(arrayToStoreIntValOfChars[charAt]){
				return false;
			}else{
			arrayToStoreIntValOfChars[charAt]=true;
			}
		}
		return true;
	}
}


	
	
	
	