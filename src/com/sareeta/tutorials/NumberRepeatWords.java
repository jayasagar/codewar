package com.sareeta.tutorials;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class NumberRepeatWords {

	public static void main(String[] args) {
		repeatWords("My name My name is is Sareeta");

	}

	public static void repeatWords(String str){
		Map<String,Integer> map= new HashMap<String,Integer>();
		for(int i=0;i<str.length();i++){
			String temp="";
			do{
				temp=temp+str.charAt(i);
			}while(str.charAt(i)!=' ');

			if(map.containsKey(temp)){
				int inte=map.get(temp);
			map.put(temp, inte+1);
			}else map.put(temp, 1);
		}
		
		Iterator it = (Iterator) map.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        System.out.println(pairs.getKey() + " = " + pairs.getValue());
	    }		
	}
}
