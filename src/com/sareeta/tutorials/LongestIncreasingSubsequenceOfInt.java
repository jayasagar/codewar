package com.sareeta.tutorials;
/**
 * @author sareetasimanchalapanda
 *
 */
public class LongestIncreasingSubsequenceOfInt {

	public static void main(String[] args) {
		int[] arrayOfInt = { 10, 3, 1, 9, 11, 13, -1, 0, 2, 5, 6, 4 };
		int[] result = findLongestIncreasingSubsequenceForIntegers(arrayOfInt);
		System.out.println("Longest sequence starts from index: " + result[0]);
		System.out.println("Length of the longest susequence is : " + result[1]);
	}

	/**
	 * This method returns an array with start index value of the max length
	 * substring, in 0th position, and the length of the longest increasing
	 * subsequence in the 1st position.
	 * 
	 * Time complexity is O(n)
	 * 
	 * @param arrayOfInt
	 * @return
	 */
	private static int[] findLongestIncreasingSubsequenceForIntegers(int[] arrayOfInt) {
		int[] resultArray = new int[2];
		int arrLength = arrayOfInt.length;
		// check if input int array length is > 0 otherwise return empty array
		// result
		if (arrayOfInt != null && arrLength > 0) {
			// default current element is the 1st element of the input array so
			// 0th index
			int currentElement = arrayOfInt[0];
			int maxLengthOfLongestIncreasingSubsequence = 1;
			int startIndexOfLongestIncreasingSubsequence = -1;
			for (int i = 0; i < arrLength - 1; i++) {
				// check current element with next adjacent element, if the current element is less than the adjacent element, increase
				// the max length and change the current element to next adjacent element
				if (currentElement < arrayOfInt[i + 1]) {
					maxLengthOfLongestIncreasingSubsequence++;
					// assign start index value only if the start index is -1
					if (startIndexOfLongestIncreasingSubsequence == -1) {
						startIndexOfLongestIncreasingSubsequence = i;
					}
					currentElement = arrayOfInt[i + 1];
				} else {
					// reset current element
					currentElement = arrayOfInt[i + 1];
					// if the start index is > -1 check whether the already existing result max length is less than current found subsequent 
					// max length, if yes replace existing result with the current value
					if (startIndexOfLongestIncreasingSubsequence > -1) {
						if (resultArray[1] < maxLengthOfLongestIncreasingSubsequence) {
							resultArray[0] = startIndexOfLongestIncreasingSubsequence;
							resultArray[1] = maxLengthOfLongestIncreasingSubsequence;
						}
					}
					// resetting here
					maxLengthOfLongestIncreasingSubsequence = 1;
					startIndexOfLongestIncreasingSubsequence = -1;
				}
			}
		}
		return resultArray;
	}

}
