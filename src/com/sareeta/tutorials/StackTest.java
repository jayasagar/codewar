package com.sareeta.tutorials;

public class StackTest {

	public static void main(String[] args) {
	Stack<Integer> stExample = new Stack<Integer>(2);
	
	System.out.println("Is stack empty at this point:"+ stExample.isEmpty());
	System.out.println("Was Pop Successful:"+stExample.pop());
	//System.out.println("Insert 2:"+stExample.push(""));
	System.out.println("Insert 3:"+stExample.push(3));
	System.out.println("Is Insert 4 possible?:"+stExample.push(4));
	System.out.println("Is stack full at this point:"+ stExample.isFull());
	System.out.println("Current state");
	stExample.display();
	
	int u  = (Integer)stExample.pop();

	}

}
