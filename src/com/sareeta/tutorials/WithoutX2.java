package com.sareeta.tutorials;

/*
 * Given a string, if one or both of the first 2 chars is 'x', return the string without those 'x' chars, and otherwise return the string unchanged. This is a little harder than it looks. 

 withoutX2("xHi") → "Hi"
 withoutX2("Hxi") → "Hi"
 withoutX2("Hi") → "Hi"
 */
public class WithoutX2 {

	public static void main(String[] args) {
		System.out.println(withoutX("xHix"));
	}

	public static String withoutX(String str) {
		if (str.length() > 0 && str.charAt(0) == 'x') {
			str = str.substring(1);
		}

		if (str.length() > 0 && str.charAt(str.length() - 1) == 'x') {
			str = str.substring(0, str.length() - 1);
		}
		return str;
	}

}
