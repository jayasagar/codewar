package com.sareeta.datastructure.tree;

import com.sareeta.datastructure.stack.Stack;

public class BinarySearchTreeImpl {
	Node root;

	public boolean insert(int data) {
		if (root == null) {
			root = new Node(data);
			return true;
		}
		insert(root, data);
		return true;

	}

	public void insert(Node node, int data) {
		if (data < node.getValue()) {
			if (node.left == null)
				node.left = new Node(data);
			else
				insert(node.left, data);
		} else if (data > node.getValue()) {
			if (node.right == null)
				node.right = new Node(data);
			else
				insert(node.right, data);
		}
	}

	public void inOrderTraverse() {

		if (root == null)
			return;
		inOrderHelper(root);

	}

	private void inOrderHelper(Node node) {
		if (node.left != null) {
			inOrderHelper(node.left);
		}
		System.out.println("In-order:" + node.getValue());
		if (node.right != null) {
			inOrderHelper(node.right);
		}
	}

	public void preOrderTraverse() {

		if (root == null)
			return;
		preOrderHelper(root);

	}

	private void preOrderHelper(Node node) {
		System.out.println("Pre-order:" + node.getValue());
		if (node.left != null) {
			preOrderHelper(node.left);
		}
		if (node.right != null) {
			preOrderHelper(node.right);
		}
	}

	public void postOrderTraverse1() {

		if (root == null)
			return;
		postOrderHelper(root);

	}

	private void postOrderHelper(Node node) {
		if (node.left != null) {
			postOrderHelper(node.left);
		}
		if (node.right != null) {
			postOrderHelper(node.right);
		}
		System.out.println("Post-order:" + node.getValue());
	}

	public void noSiblingList() {
		if (root == null)
			return;
		doesNodeHaveSibling(root);
	}

	private void doesNodeHaveSibling(Node node) {
		if (node.left != null && node.right != null) {
			doesNodeHaveSibling(node.left);
			doesNodeHaveSibling(node.right);
		} else if (node.left != null) {
			System.out.println(node.left.getValue());
			doesNodeHaveSibling(node.left);
		} else if (node.right != null) {
			System.out.println(node.right.getValue());
			doesNodeHaveSibling(node.right);
		}
	}

	public boolean isElementPresent(int input) {
		if (root == null)
			return false;
		return elementSearchHelper(root, input);

	}

	private boolean elementSearchHelper(Node node, int input) {
		if (node.getValue() == input)
			return true;
		else if (input > node.getValue()) {
			if (node.right == null)
				return false;
			else
				return elementSearchHelper(node.right, input);
		} else {
			if (node.left == null)
				return false;
			else
				return elementSearchHelper(node.left, input);
		}
	}

	public void printLeafNodes() {
		if (root == null) {
			System.out.println("Root node is the leaf node");
			return;
		}
		isLeafNode(root);
	}

	private void isLeafNode(Node node) {
		if (node.left == null && node.right == null) {
			System.out.println(node.getValue());
		}
		if (node.left != null) {
			isLeafNode(node.left);
		}
		if (node.right != null) {
			isLeafNode(node.right);
		}
	}

}