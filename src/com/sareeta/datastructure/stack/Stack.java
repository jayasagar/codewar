package com.sareeta.datastructure.stack;

public class Stack {
	private Node topNode = null;

	public void push(Object elementToPush) {

		Node node = new Node(elementToPush);
		if (topNode != null) {
			node.setPrevious(topNode);
		}
		topNode = node;
	}

	public Object pop() {
		if (!isEmpty()) {
			Node temp = topNode;
			topNode = topNode.getPrevious();
			return temp.getData();
		} else {             
			return -1;
		}
	}
	
	public boolean isEmpty() {
		return (topNode == null);
	}

	public void display() {
		Node temp = topNode;
		while(temp!=null){
			System.out.println("Value: "+ temp.getData());
			temp = temp.getPrevious();
		}	
	}

}
