package com.sareeta.datastructure.stack;

public class Node {
	private Object data;
	private Node previous;
	
	public Node getPrevious() {
		return previous;
	}

	public void setPrevious(Node previous) {
		this.previous = previous;
	}

	public Object getData() {
		return data;
	}

	Node(Object data){
		this.data = data;
	}
	
	
}
