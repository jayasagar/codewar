package com.sareeta.datastructure.List;

public class ArraylistImpl {
	private Object[] obj;
	private int index;

	public ArraylistImpl(int sz) {
		obj = new Object[sz];
	}

	public void add(Object objectToAdd) {
		if (!isFull()) {
			obj[index++] = objectToAdd;
		} else {
			// method to increase space
			// increaseSpace();
		}
	}

	private boolean isFull() {
		return obj.length == getSize();
	}

	public boolean remove(int val) {
		if (!isEmpty()) {
			for (int i = val; i < getSize() - 1; i++) {
				obj[i] = obj[i + 1];
			}
			index--;
			return true;
		} else
			return false;
	}

	public boolean contains(Object objectToCheck) {
		for (int i = 0; i < getSize(); i++) {
			if (obj[i].equals(objectToCheck))
				return true;
		}
		return false;
	}

	public boolean isEmpty() {
		return getSize() == 0;
	}

	public int getSize() {
		return index;

	}

	public void display() {
		for (int i = 0; i < getSize(); i++) {
			System.out.println(obj[i]);
		}
	}

}
