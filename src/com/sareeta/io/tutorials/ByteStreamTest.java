package com.sareeta.io.tutorials;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

//buffer reader and bufferwriter
public class ByteStreamTest {

	public static void main(String[] args) throws IOException {
		System.out.println(System.getProperty("user.test"));
		System.out.println(System.getProperty("user.dir"));
		File file = new File("test.txt");
		OutputStream outStream = new FileOutputStream(file);
		outStream.write("sareeta".getBytes());
		outStream.close();
		charStream();
		bufferStream();
	}
	private static void bufferStream() throws IOException {
		File file = new File("test2.txt");
		@SuppressWarnings("resource")
		BufferedOutputStream oStream = new BufferedOutputStream(new FileOutputStream(file));
		oStream.write("sareeta".getBytes());
		oStream.flush();
		oStream.close();
	}
	
	
	private static void charStream() throws IOException {
		File file = new File("test1.txt");
		Writer fw = new FileWriter(file);
		fw.write("Sareeta");
		fw.write("\nPanda");
		fw.close();
		Reader reader = new FileReader("test1.txt");
		StringBuilder nString = new StringBuilder();
		/*char[] ch = new char[(int) file.length()];
		reader.read(ch);*/
		int temp = 0;
		while((temp=reader.read())!=-1){
			nString.append((char)temp);
		}
		System.out.println(nString);
		reader.close();
		fw.close();
	}
}
