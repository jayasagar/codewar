package com.sareeta.threads;

import java.io.Serializable;

public class SimpleThread {

	public static void main(String[] args) throws InterruptedException {
		Thread t = new Thread() {
			boolean stop = false;
			@Override
			public void run() {
				for (int i = 0; i <= 5; i++) {
					System.out.println(Thread.currentThread().getName()+ " "+i);
				}
				if (stop) {
					return ;
				}
				
			}
		};
		t.setPriority(Thread.MIN_PRIORITY);
		t.start();
		t.join();
		System.out.println("Try");
		//SubThread.start();
		/*Serializable sz = new Serializable() {};
		m(sz);*/
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	static void m(Serializable sz) {
		
	}
}

class SubThread extends Thread {
	@Override
	public void run() {
		super.run();
	}
}