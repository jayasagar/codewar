package com.sareeta.threads;

public class RunOddEven {

	public static void main(String[] args) throws InterruptedException {

		Integer i = 5;
		Thread even = new Thread(new EvenResultRunnable(i));
		Thread odd = new Thread(new OddResultRunnable(i));
		odd.start();
		even.start();
	}
}
