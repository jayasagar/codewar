package com.sareeta.threads;

public class ThreadStackTest {

	public static void main(String[] args) {
		final Stack<Integer> s1 = new Stack<Integer>(6);
		final Stack<Integer> s2 = new Stack<Integer>(6);
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				s1.push(1);
				s1.push(2);
				s1.push(4);
				s1.push(6);
				s1.display();
			}
		});
		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				s1.push(7);
				s1.push(5);
				s1.display();
				s1.push(9);

			}
		});
		t1.start();
		t2.start();
	}
}
