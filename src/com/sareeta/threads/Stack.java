package com.sareeta.threads;

//Use Generics, Add exceptions, Multithread, memory leak, cloneable,serializable

public class Stack<E> {
	private final Object[] elements;
	private int top = -1;

	public Stack(int capacity) {
		elements = new Object[capacity];
	}

	// insert elements
	public synchronized boolean push(E eleToPush) {
		if (isFull()) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		elements[++top] = eleToPush;
		this.notify();
		return true;
	}

	// remove elements,returns -1 when empty
	public synchronized E pop() throws InterruptedException {
		if (isEmpty()) {
			this.wait();
		}
			E temp = (E) elements[top];
			elements[top] = null;
			top--;
			this.notify();
			return temp;
		}

	// check if no elements
	public synchronized boolean isEmpty() {
		return (top == -1);
	}

	// check if full
	public synchronized boolean isFull() {
		return (top == elements.length - 1);
	}

	public void display() {
		for (int i = 0; i < elements.length; i++) {
			System.out.println(elements[i]);
		}

	}

}
