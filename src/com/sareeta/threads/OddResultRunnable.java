package com.sareeta.threads;

public class OddResultRunnable implements Runnable {
	int range;

	@Override
	public void run() {
		for (int i = 1; i <= range; i = i + 2) {
			System.out.println("Odd Thread:" + i);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public OddResultRunnable(int range) {
		this.range = range;
	}

}
