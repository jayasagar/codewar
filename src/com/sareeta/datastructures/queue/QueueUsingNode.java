package com.sareeta.datastructures.queue;

public class QueueUsingNode {
	
private class Node{
	public Node next;
	public int data;
	public Node(int data, Node next){
		this.next=next;
		this.data=data;
	}
	
}

private Node head = null;
private Node tail = null;

	public void enqueque(int data){
		Node newNode = new Node(data, null);
		if(isEmpty()) {
			head = newNode;
		}else {
		tail.next = newNode;
		}
		tail = newNode;
	}
	private boolean isEmpty() {
		return head==null;
	}

	
	public int dequeque() {
		if(isEmpty()) throw new IllegalStateException();
		int temp=head.data;
		if(tail == head) tail = null;
		head= head.next;
		return temp;
		
	}
}
