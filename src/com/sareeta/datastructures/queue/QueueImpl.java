package com.sareeta.datastructures.queue;

public class QueueImpl{
	private int first=0, next=-1;
	private final int ary[];
	
	public QueueImpl(int capacity){
		ary = new int[capacity];
	}
	
	public boolean add(int ele){
	ary[++next] = ele;
	return true;
	}
	
	public int poll(){
		return ary[first++];
	}
	
	//contains
	public void display(){
	for(int i=0; i<ary.length;i++){
		System.out.println(ary[i]);
	}
}
	
}
